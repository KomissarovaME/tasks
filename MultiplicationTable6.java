package tasks;

import java.util.Scanner;

public class MultiplicationTable6 {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        int number = scanner.nextInt();
        for(int i = 1; i < 11; i++) {
            System.out.printf("%d * %d = %d%n", number, i, number * i);
        }
    }
}
