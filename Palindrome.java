package tasks;

import java.util.Scanner;

/**
 * Класс определяющий является ли слово или же число палиндромом.
 *
 * @author Komissarova M.E
 */

public class Palindrome {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Здравствуйте! Эта программа определяет является ли словосочетание полиндромом.");
        String word = scanner.nextLine().toLowerCase();
        space(word);
        word = word.replaceAll("[^a-zа-я0-9]", "");
        String palindrome = new StringBuffer(word).reverse().toString();
        word(word, palindrome);
    }

    /**
     * Метод space при введении пустой строки выводит сообщение о некорректности данных.
     *
     * @param word
     */
    private static void space(String word) {
        while (word.equals("")) {
            System.out.println("Данные введены некорректно. Повторите ввод.");
            word = scanner.nextLine().toLowerCase();
        }
    }

    /**
     *Метод word получает на вход данные параметра word и условие palindrom и после прохождения выводит сообщение.
     *
     * @param word
     * @param palindrome
     */
    private static void word(String word, String palindrome) {
        if (word.equals(palindrome)) {
            System.out.println("Палиндром.");
        } else {
            System.out.println("Не палиндром.");
        }

    }
}