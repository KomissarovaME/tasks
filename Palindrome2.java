package tasks;

import java.util.Scanner;

/**
 * Класс определяющий является ли слово палиндромом.
 *
 * @author Komissarova M.E
 */

public class Palindrome2 {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Здравствуйте! Эта программа определяет является ли словосочетание полиндромом.");
        String word = scanner.nextLine().toLowerCase();
        while (word.equals(" ")){
            System.out.println("Данные некорректны. Поробуйте снова.");
            word = scanner.nextLine().toLowerCase();
        }
        word = word.replaceAll("[^a-zа-я]", "");
        String palindrome = new StringBuffer(word).reverse().toString();
        word(word, palindrome);
    }
    /**
     *Метод word получает на вход данные параметра word и условие palindrom2 и после прохождения выводит сообщение.
     *
     * @param word
     * @param palindrome
     */
    private static void word(String word, String palindrome) {
        if (word.equals(palindrome)) {
            System.out.println("Палиндром.");
        } else {
            System.out.println("Не палиндром.");
        }

    }
}
