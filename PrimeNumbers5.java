package tasks;

public class PrimeNumbers5{
    private static final int MIN = 2, MAX = 100;

    public static void main(String[] args) {
        boolean isSimple = false;
        for(int i = MIN; i < MAX; i++) {
            if(i < 7) {
                    isSimple = (i != 4 && i != 6);
            } else {
                    isSimple = (i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0);
                }

            if(isSimple) {
                System.out.println(i);
            }
        }
    }
}
