package tasks;

public class Transposition {
    public static void main(String[] args) {
        int a = 5;
        int[][] matrixA = new int[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                matrixA[i][j] = 1 + (int) (Math.random() * 9);
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("А теперь транспонируем:");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print(matrixA[j][i] + "\t");
            }
            System.out.println();
        }
    }
}
