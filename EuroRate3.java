import java.util.Scanner;

public class EuroRate3 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите курс евро по отношению к рублю (сколько рублей стоит 1 евро): ");
        double course = readCorrect();
        System.out.print("Введите количество рублей: ");
        double roubles = readCorrect();
        System.out.printf("За %.3f рублей можно купить %.3f евро%n", roubles, roubles / course);
    }

    public static double readCorrect() {
        double v = scanner.nextDouble();
        while(v < 0) {
            System.out.print("Похоже, вы допустили ошибку! Попробуйте еще раз: ");
            v = scanner.nextDouble();
        }
        return v;
    }
}
