package tasks;

import java.util.Scanner;

public class Matrix8 {

    private static Scanner scanner = new Scanner(System.in);

    private static int[][] matrix = new int[3][3];

    public static void main(String[] args) {
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                    matrix[i][j] = (int)(Math.random() * 100);
                System.out.printf("%3.0f ", matrix[i][j]);
            }
            System.out.println();
        }

        int column = scanner.nextInt();
        clearColumn(column);
    }

    public static void clearColumn(int col) {
        matrix[col] = new int[]{ 0, 0, 0 };
    }
}

