package tasks;

import java.util.Scanner;

public class Timing11 {
     public static void main(String[] args) {
         Scanner reader = new Scanner(System.in);
         System.out.println("Введите кол-во дней.");
         int days = reader.nextInt();
         System.out.println("В часах: " + days * 24);
         System.out.println("В минутах: " + days * 24 * 60);
         System.out.println("В секундах: " + days * 24 * 3600);
     }
}
