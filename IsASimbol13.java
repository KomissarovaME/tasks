package tasks;
import java.util.Scanner;

public class IsASimbol13 {

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String args[]){
        String input;
        do {
            System.out.println("Введите символ");
            input = scanner.nextLine();
            if(input.length() > 1 || input.length() == 0){
                System.out.println("Error 404");
            }
        }while (input.length() > 1 || input.length() == 0);
        char ch = input.charAt(0);
        if(Character.isDigit(ch)){
            System.out.println("Число");
        }else if(Character.isLetter(ch)){
            System.out.println("Буква");
        }else if(ch == '!' || ch == '"' || ch == ':' || ch == ';' || ch == ',' || ch == '.' || ch == '?'){
            System.out.println("Знак пунктуации");
        }else {
            System.out.println("Символ на является числом, буквой или знаком пунктуации!");
        }
    }
}
